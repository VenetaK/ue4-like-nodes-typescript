module.exports = {
    watch: true,
    entry: './src/app.ts',
    output: {
        filename: 'bundle.js',
        path: './bundle'
    },
    resolve: {
        extensions: ['', '.webpack.js', '.web.js', '.ts', '.js']
    },
    module: {
        loaders: [
            { test: /\.ts$/, loader: 'ts-loader' }
        ]
    }
}
