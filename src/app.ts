import * as hammer from 'hammerjs';
import Node from './Node';
import Itempos from './Position';
import Renderer from './Renderer';
import NodeSocket from './NodeSocket';
import Arrow from './Arrow';
import DisplayObject from './displayObject';

class App {

    public canvas: HTMLCanvasElement;
    public context: CanvasRenderingContext2D
    public displayObjects: Array<DisplayObject>;
    public doomhammer: any;
    public renderer: Renderer;
    public arrow: Arrow;

    private frame_rate: number;

    constructor(canvas: HTMLCanvasElement, nodes: Array<Node>) {
        this.frame_rate = 1000 / 60;
        this.canvas = canvas;
        this.context = canvas.getContext('2d');
        this.canvas.style.backgroundColor = '#33cc14';

        this.doomhammer = new hammer(canvas);
        this.configDoomhammer();
        this.displayObjects = nodes;
        this.renderer = new Renderer(this.context);
    }
    /**
     * sets up hammerjs's pan event to recognize all directions
     *  */
    configDoomhammer() {
        this.doomhammer.get('pan').set({
            direction: Hammer.DIRECTION_ALL
        })
    }

    /**
     * checks if the mouse is on a node, if so, it checks if the mouse is 
     * on one of the node's sockets
     * 
     * @param e - {Object} 
     *  */
    onPanStart(e) {
        var mouse = new Itempos(e.srcEvent.offsetX, e.srcEvent.offsetY);
        let node: DisplayObject = this.getCollision(mouse);
        if (node) {
            var startDrag = new Itempos(mouse.x - node.position.x, mouse.y - node.position.y);
            let foundSocket: NodeSocket | null = node.hoversSocket(mouse);
            if (foundSocket) {
                this.arrow = this.createArrow(mouse, foundSocket);
                this.displayObjects.push(this.arrow);
            } else {
                node.setDraggable(startDrag);
            }
        }
    }

    createArrow(mousePos: Itempos, foundSocket: NodeSocket) {
        return new Arrow(mousePos, new Itempos(5, 5), foundSocket);
    }

    getCollision(mousePos: Itempos): DisplayObject | null {
        for (var i in this.displayObjects) {
            if (this.displayObjects[i].hovers(mousePos)) return this.displayObjects[i];
        }
        return null;
    }

    onPanMove(e) {
        var mousePos = new Itempos(e.srcEvent.offsetX, e.srcEvent.offsetY);
        for (var i in this.displayObjects) {
            if (!this.displayObjects[i].drag) continue;
            var dragPos = new Itempos(mousePos.x - this.displayObjects[i].startDrag.x, mousePos.y - this.displayObjects[i].startDrag.y);
            this.displayObjects[i].move(dragPos);
        }
    };

    unsetDraggable() {
        for (let i in this.displayObjects) {
            this.displayObjects[i].drag = false;
        }
    }

    removeArrow() {
        if (this.arrow) {
            let index = this.displayObjects.indexOf(this.arrow);
            this.displayObjects.splice(index, 1);

            this.arrow = null;
        }
    }

    onPanEnd(e) {
        var mouse = new Itempos(e.srcEvent.offsetX, e.srcEvent.offsetY);
        this.unsetDraggable();

        let node = this.getCollision(mouse);
        if (node) {
            let foundSocket = node.hoversSocket(mouse);
            if (foundSocket && this.arrow) {
                if (this.arrow.drop(foundSocket)) {
                    this.arrow = null;
                } else {
                    this.removeArrow();
                }
            } else if (!foundSocket) {
                this.removeArrow();
            }
        } else {
            this.removeArrow();
        }
    }

    tick() {
        setInterval(() => {
            this.renderer.draw(this.displayObjects);
        }, this.frame_rate);
    }

    start() {
        this.doomhammer.on('panstart', this.onPanStart.bind(this));
        this.doomhammer.on('panmove', this.onPanMove.bind(this));
        this.doomhammer.on('panend', this.onPanEnd.bind(this));

        this.tick();
    }
}

let canvas = <HTMLCanvasElement>document.getElementById('canvas');
let node = new Node(new Itempos(40, 40), new Itempos(150, 150));
//manually create and add nodes
let node2 = new Node(new Itempos(250, 300), new Itempos(150, 150));
let node3 = new Node(new Itempos(400, 100), new Itempos(150, 150));

node.addIoNode(new NodeSocket(new Itempos(150, 60), new Itempos(35, 35)));
node.addIoNode(new NodeSocket(new Itempos(150, 60), new Itempos(35, 35), 'triangle'));
node.addIoNode(new NodeSocket(new Itempos(150, 60), new Itempos(35, 35), 'hexagon'));

node3.addIoNode(new NodeSocket(new Itempos(150, 60), new Itempos(35, 35), 'triangle'));
node3.addIoNode(new NodeSocket(new Itempos(150, 60), new Itempos(35, 35)));
node3.addIoNode(new NodeSocket(new Itempos(150, 60), new Itempos(35, 35), 'hexagon'));



node2.addIoNode(new NodeSocket(new Itempos(150, 60), new Itempos(35, 35)));
node2.addIoNode(new NodeSocket(new Itempos(150, 60), new Itempos(35, 35), 'triangle'));
node2.addIoNode(new NodeSocket(new Itempos(150, 60), new Itempos(35, 35), 'hexagon'));



let app = new App(canvas, [node, node2, node3]);
app.start();

