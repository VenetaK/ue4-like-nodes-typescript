import Itempos from './Position';

class DisplayObject {
    public size: Itempos;
    public position: Itempos; 
    public startDrag: Itempos;
    public drag: boolean = false;

    constructor (position: Itempos, size: Itempos) {
        this.position = position;
        this.size = size;
        this.startDrag = new Itempos(0, 0);
    }

    draw (context: CanvasRenderingContext2D) {
        
    }

    move (position: Itempos) {
        this.position = position;
    }

    hovers (mousePos: Itempos) {
        let hovers = false;

        let right = this.position.x + this.size.x;
        let bottom =  this.position.y + this.size.y;
        let top =  this.position.y;
        let left = this.position.x;

        if(mousePos.x < right && mousePos.x > left && mousePos.y < bottom && mousePos.y > top){
            hovers = true;   
        }

        return hovers;
    }


    setDraggable (startDrag: Itempos) {
        this.startDrag = startDrag;
        this.drag = true;
    }

    hoversSocket (mousePos: Itempos) {
        return null;    
    }
}

export default DisplayObject; 