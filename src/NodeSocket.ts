import Node from './Node';
import Itempos from './position';
import DisplayObject from './DisplayObject'

class NodeSocket extends DisplayObject {
    public parent: Node;
    public type: string;
    public displayed: boolean;
    public vertices: Object;
    constructor(position: Itempos, size: Itempos, type?: string) {
        super(position, size);
        this.displayed = false;
        this.type = type || 'square';
        this.vertices = {};
    }

    setPosition() {
        let parentRight = this.parent.position.x + this.parent.size.x;
        this.position.x = parentRight - 20 - this.size.x;
        this.position.y = this.parent.position.y + 10;

        //if there are other sockets, this one should go beneith them
        for (let i in this.parent.node_sockets) {
            let socket = this.parent.node_sockets[i];
            if (!socket.displayed) continue;
            this.position.y += socket.size.y + 10;
        }
    };

    drawTriangle(context: CanvasRenderingContext2D) {
        let halfWidth = this.size.x / 2;
        let startX = this.position.x + halfWidth;
        let startY = this.position.y + this.size.y;

        let c = [startX, this.position.y];
        let a = [startX - halfWidth, startY];
        let b = [((startX - halfWidth) + this.size.x), startY];

        context.beginPath();
        context.lineTo(c[0], c[1]);
        context.lineTo(a[0], a[1]);
        context.lineTo(b[0], b[1]);
        context.fillStyle = 'black';
        context.fill();
        context.closePath();
    }

    drawRectangle(context: CanvasRenderingContext2D) {
        this.setPosition();
        context.beginPath();
        context.rect(this.position.x, this.position.y, this.size.x, this.size.y);
        context.stroke();
        context.closePath();
        this.displayed = true;
    }

    drawHexagon(context: CanvasRenderingContext2D) {
        let fourth = this.size.x / 4;
        let half = this.size.y / 2; 
        let x = this.position.x;
        let y = this.position.y;

        context.beginPath();
        context.moveTo(x + fourth, y);
        context.lineTo(x + this.size.x - fourth, y);
        context.lineTo(x + this.size.x, y + half);
        context.lineTo(x + this.size.x - fourth, y + this.size.y);
        context.lineTo(x + fourth, y + this.size.y);
        context.lineTo(x, y + half);
        context.lineTo(x + fourth, y);
        context.stroke();
        context.closePath();
    }

    draw(context: CanvasRenderingContext2D) {
        this.setPosition();

        switch (this.type) {
            case 'triangle':
                this.drawTriangle(context);
                break;
            case 'hexagon':
                this.drawHexagon(context);
                break;
            default: //sqare 
                //draw sqare
                this.drawRectangle(context);
                break;
        }
        this.displayed = true;
    }
}

export default NodeSocket;