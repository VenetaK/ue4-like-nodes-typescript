import DisplayObject from './DisplayObject';
import Itempos from './Position';
import NodeSocket from './NodeSocket'


class Arrow extends DisplayObject {
    public startSocket: NodeSocket;
    public endSocket: NodeSocket;
    public socketType: string;
    public drag: boolean;

    constructor(position: Itempos, size: Itempos, startSocket: NodeSocket) {
        super(position, size);
        this.drag = true;
        this.startSocket = startSocket;
    }

    drop(endSocket: NodeSocket) {
        if (this.startSocket.type === endSocket.type) {
            this.endSocket = endSocket;
            // this.position = this.endSocket.position;
            return true;
        }
        return false;
    }

    getEndPosition() {
        if (this.endSocket) {
            return this.endSocket.position;
        }

        return this.position;
    }

    draw(context: CanvasRenderingContext2D) {
        let endPosition = this.getEndPosition();
        let socketSize = this.endSocket?this.endSocket.size.x / 2:0; 
        context.beginPath();
        context.moveTo(this.startSocket.position.x + (this.startSocket.size.x) / 2, this.startSocket.position.y + (this.startSocket.size.y) / 2);
        context.lineTo(endPosition.x + socketSize, endPosition.y + socketSize); 
        context.lineWidth = 3;
        context.stroke();
        context.closePath();
        context.lineWidth = 1;
    }
}

export default Arrow;