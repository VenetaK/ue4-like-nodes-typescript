class Itempos {
    x: number;
    y: number;

    constructor (x: number, y: number) {
        this.setPosition(x, y);
    }

    getPosition () {
        return {
            x: this.x,
            y: this.y
        };
    }

    setPosition (x: number, y: number) {
        this.x = x;
        this.y = y
    }

    sum (a: number, b: number) {
        return a + b;
    }

    diff (a: number, b:number) {
        return a - b;
    }
}

export default Itempos;