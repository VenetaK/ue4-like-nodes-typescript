import DisplayObject from './DisplayObject';

class Renderer {

    context: CanvasRenderingContext2D;
    
    constructor (context: CanvasRenderingContext2D) {
        this.context = context;
    }
    /**
     * draws the DisplayObjects onto the canvas by calling 
     * each of the DisplayObject's draw methos
     * 
     * @param displayObject {Array<DisplayObject>} 
     */
    draw (displayObjects:  Array<DisplayObject>) {
        this.context.clearRect(0, 0, this.context.canvas.width, this.context.canvas.height);

        for(let i in displayObjects){
            let displayObject: DisplayObject =  displayObjects[i];
            displayObject.draw(this.context);
        }
    }
}

export default Renderer;