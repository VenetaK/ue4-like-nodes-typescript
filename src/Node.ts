import Itempos from './Position';
import NodeSocket from './NodeSocket';
import DisplayObject from './DisplayObject';

class Node extends DisplayObject{
    public node_sockets: Array<NodeSocket>;
    private fillStyle: string;

    constructor (position: Itempos, size: Itempos) {
        super(position, size);
        this.node_sockets = [];
        this.fillStyle = '#4286f4';
    }

    addIoNode (nodeSocket: NodeSocket) {
        nodeSocket.parent = this;
        this.node_sockets.push(nodeSocket);
    }

    hoversSocket (mousePos: Itempos) {
        for (let i=0;i<this.node_sockets.length;i++) {
            if (!this.node_sockets[i].hovers(mousePos)) continue;
            return this.node_sockets[i];
        }
        return null;
    }

    resetSockets () {
        for (var i in this.node_sockets) {
            this.node_sockets[i].displayed = false;
        }
    }
    
    drawSockets (context: CanvasRenderingContext2D) {
        for (var i in this.node_sockets) {
            this.node_sockets[i].draw(context);
        }
    }

    draw (context: CanvasRenderingContext2D) {
        context.beginPath();
        context.rect(this.position.x, this.position.y, this.size.x, this.size.y);
        context.stroke();
        context.fillStyle = this.fillStyle;
        context.fill();
        context.closePath();
        this.resetSockets();
        this.drawSockets(context);
    }

    move (position: Itempos) {
        this.position = position;
    }
}

export default Node;